
# N1 (Notice of Rent Increase) Form Writer

Ontario rent increases require these specially-formatted [N1 forms](http://www.sjto.gov.on.ca/ltb/forms/) that kind of
suck to write. This does that for you.

## Usage

First, download the script and related files, then edit info.ini to look
something like this (there are more instructions in the file itself):

```ini
[program]
out_path      = /mnt/c/Users/Briteside Management/Desktop/Completed_N1s/
font_path     = ./Resources/UbuntuMono-R.ttf

[info]
increase_pct  = 1.8
increase_date = auto:95
landlord      = Briteside Apartments
landlord_addr = 130 Queen Street, London, Ontario. B3H 2R9
signatory     = Yousif Avidan
phone_no      = (555) 552 415
date          = November 7th, 2018
tenant_addr   = 130 Queen Street, London, Ontario. B3H 2R9
```

Then open apt\_numbers is something like Notepad++ and write information on each
unit in this format (this can easily be automated, of course):

> \<unit\> : \<tenant name\> : \<current monthly rent\>

your file should then look something like

> A203 : Arnold Hemsway : 900  
> A204 : Alexy Gorchev : 800  
> A205 : Charley Kissenger : 750  
> A206 : David Zimmerman : 750  
>    ... (and etc.)

Then run the program. It requires Python 3.6, as well as the packages *Pillow* and
*pendulum*.

It will output two pages for each unit, the second page being the same each time.
The reason this second page is recreated every time is to allow all the files to
be combined as one big document in, say, Acrobat, then printed in double sided
mode.

Example N1:

| Page 1             |  Page 2 |
|:-------------------------:|:-------------------------:|
|![N1 page 1](Example_images/N1-A203.png) | ![N1 page 2](Example_images/N1-A203-2.png)|
