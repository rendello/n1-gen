#!/usr/bin/python3.6

from math import floor
from os import path as _path  # avoid naming conflict
from PIL import Image, ImageFont, ImageDraw 

import configparser
import pendulum

'''
    This script was made to simplify writing a lot of N1 forms (Ontario Notice
    of Rent Increase). Of course, they still need to be signed and all that.
'''

def expand_relpath(relpath):
    ''' takes a path beginning with a dot and expands the dot to the absolute
        path of the software.
        
        ./UbuntuMono-R.ttf > /home/Gaven/Desktop/N1_Software/UbuntuMono-R.ttf
    '''

    global path

    if relpath.startswith('.'):
        relpath = relpath.strip('.')
        abspath = f'{path}{relpath}'
        
        return abspath
    return relpath


def add_date(c_date, n_date):
    ''' Deals with dates of increase. If a date in the format DD-MM-YYYY (not
        my choice) is given, the program will pass it along.
        
        If the special 'auto' value is given, it automatically adds 90 days to
        the given current date. Another number of days can be given with the
        'auto:[number of days]' format.

        If it's not the first of the month, it will set for the first of the
        following month.

        current     new          returns ... 
        16-11-2018  17-11-2018 > 01-12-2018
        16-11-2018  auto       > 01-03-2019
        16-11-2018  auto:105   > 01-03-2019
        16-11-2018  auto:106   > 01-04-2019
    '''
    if 'auto' in n_date:
        days_to_add = 90
        if 'auto:' in n_date:
            days_to_add = int(n_date.split(':')[1])


        dt = pendulum.from_format(date, 'DD-MM-YYYY')
        dt = dt.add(days=days_to_add)

        if dt.day != 1:
            # default to first of next month
            dt = dt.add(months=1)
            dt = dt.subtract(days=dt.day - 1)

        return dt.format('DD-MM-YYYY')
    return n_date


def text(location, string):
    ''' A simplified draw.text() '''
    draw.text(location, string ,(0, 0, 0), font=font)


def prepare_for_boxes(string, box_length):
    ''' Inserts spaces between letters, so the monospaced letters can fit well
        in the boxed sections in "Your New Rent" and "Explanation of The Rent
        Increase". Also adds padding to the left and shifts as needed.

        The box_length is the amound of spaces and lines in the box.

        '400'  > '      4 0 0', thus [ | | |4|0|0] when displayed over boxes
        '4000' > '    4 0 0 0', thus [ | |4|0|0|0] ~
    '''
    spaced_string = ''
    for letter in string:
        spaced_string += f' {letter}'

    padding = box_length - len(spaced_string)
    result = (padding * ' ') + spaced_string

    return result


path = _path.abspath(_path.dirname(__file__)) # current dir of software

config = configparser.ConfigParser()
config.read('info.ini')

# program information
out_path  = expand_relpath(config['program']['out_path'])
font_path = expand_relpath(config['program']['font_path'])

print(out_path)
print(font_path)

# apartment / increase specifics
increase_pct  = float(config['info']['increase_pct'])

landlord      = config['info']['landlord']
landlord_addr = config['info']['landlord_addr']
signatory     = config['info']['signatory']
phone_no      = config['info']['phone_no']
date          = config['info']['date']
increase_date = config['info']['increase_date']
tenant_addr   = config['info']['tenant_addr']

increase_date = add_date(date, increase_date)



with open('apt_numbers', 'r') as info:
    for line in info:
        apt_no, name, old_rate = line.split(' : ')

        apt_no = apt_no.capitalize()
        old_rate = int(old_rate)

        increase = floor(old_rate * (increase_pct / 100))
        new_rate = old_rate + increase

        # >>>  first page  <<<
        img = Image.open(f'{path}/N1_Page_1.png')
        draw = ImageDraw.Draw(img)

        font = ImageFont.truetype(font_path, 27)

        # "To: (Tenent's name and addr)"
        text((99, 312), f'{name}, Unit {apt_no},')
        text((99, 312 + 40), f'{tenant_addr}')

        # "From: (Landlord's name and addr)"
        text((879, 312), landlord)
        text((879, 312 + 40), landlord_addr)

        # "Address of the Rental Unit"
        text((99, 470), f'Unit {apt_no}, {tenant_addr}')

        font = ImageFont.truetype(font_path, 37)

        # "On dd/mm/yyyy, ..."
        display_increase_date = prepare_for_boxes(increase_date, box_length=19)
        text((435, 650), display_increase_date.replace('-', ' '))

        # "Your rent will increase to ..."
        display_rate = prepare_for_boxes(str(new_rate), box_length=11) + '   0 0'
        text((1284, 650), display_rate)

        # "This is a rent increase of: ..."
        display_increase = prepare_for_boxes(str(increase), box_length=11) + '   0 0'
        text((777, 1043), display_increase)

        img.save(f'{out_path}/N1-{apt_no}.png')

        # >>>  second page  <<<
        img = Image.open(f'{path}/N1_Page_2.png')
        draw = ImageDraw.Draw(img)

        text((112, 1240), signatory)
        text((880, 1240), phone_no)
        text((880, 1360), f'{date} (dd-mm-yyyy)')

        img.save(f'{out_path}/N1-{apt_no}-2.png')
